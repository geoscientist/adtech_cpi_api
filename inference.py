import os
import pandas as pd
from dotenv import load_dotenv
import mlflow
from fastapi import FastAPI, File, UploadFile, HTTPException


load_dotenv()

app = FastAPI()

class Model:
    '''
    Base Catboost model class
    '''
    def __init__(self, model_name: str, model_version: int):
        self.model = mlflow.pyfunc.load_model(model_uri=f"models:/{model_name}/{model_version}")

    def predict(self, data):
        '''
        Prediction method for Model class
        '''
        predictions = self.model.predict(data)
        return predictions
    
model = Model("Adtech_CPI", 5)
    

@app.post('/inference')
async def inference(file: UploadFile=File(...)):
    '''
    Predict API route
    '''
    if file.filename.endswith('.csv'):
        with open(file.filename, 'wb') as f:
            f.write(file.file.read())
        data = pd.read_csv(file.filename)
        os.remove(file.filename)
        return list(model.predict(data))
    else:
        raise HTTPException(400, detail="Invalid file format. File must have CSV extension")
    